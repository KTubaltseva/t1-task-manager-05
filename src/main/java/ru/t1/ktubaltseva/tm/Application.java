package ru.t1.ktubaltseva.tm;

import ru.t1.ktubaltseva.tm.constant.TerminalConst;

public class Application {

    public static void main(String[] args) {
        run(args);
    }

    public static void run(final String[] args) {
        if (args == null || args.length < 1) return;
        final String argument = args[0];
        switch (argument) {
            case TerminalConst.CMD_VERSION:
                displayVersion();
                break;
            case TerminalConst.CMD_ABOUT:
                displayAbout();
                break;
            case TerminalConst.CMD_HELP:
                displayHelp();
                break;
            default:
                displayError();
        }
    }

    public static void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    public static void displayAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Tubaltseva Ksenia");
        System.out.println("email: ktubaltseva@t1-consulting.ru");
    }

    public static void displayHelp() {
        System.out.println("[HELP]");
        System.out.println("version - Display program version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of terminal commands.");
    }
    public static void displayError() {
        System.err.println("Invalid command");
        displayHelp();
    }

}